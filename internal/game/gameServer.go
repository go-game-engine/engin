package game

import (
	"context"
	"errors"
	"log"
	"regexp"
	"sync"
	"time"

	"gitlab.com/go-game-engine/engin/internal/backend"
	"gitlab.com/go-game-engine/engin/internal/framework"

	"github.com/google/uuid"
	"google.golang.org/grpc/metadata"
)

const (
	clientTimeout = 15
	maxClients    = 8
)

// client contains information about connected clients.
type client struct {
	streamServer framework.Game_StreamServer
	lastMessage  time.Time
	done         chan error
	id           uuid.UUID
}

// GameServer is used to stream game information with clients.
type GameServer struct {
	framework.UnimplementedGameServer
	game    *backend.Game
	clients map[uuid.UUID]*client
	mu      sync.RWMutex
}

// NewGameServer constructs a new game server struct.
func NewGameServer(game *backend.Game) *GameServer {
	log.Println("Server: Creating New Game Server")
	server := &GameServer{
		game:    game,
		clients: make(map[uuid.UUID]*client),
	}
	server.watchTimeout()
	return server
}

func (s *GameServer) removeClient(id uuid.UUID) {
	log.Printf("Server: removing client: %s", id)
	s.mu.Lock()
	delete(s.clients, id)
	s.mu.Unlock()
}

func (s *GameServer) removePlayer(playerID uuid.UUID) {
	log.Printf("Server: removing player: %s", playerID)
	s.game.Mu.Lock()
	s.game.RemoveEntity(playerID)
	s.game.Mu.Unlock()

	/*
		resp := framework.Response{
			Action: &framework.Response_RemoveEntity{
				RemoveEntity: &framework.RemoveEntity{
					Id: playerID.String(),
				},
			},
		}
		s.broadcast(&resp)
	*/
}

func (s *GameServer) getClientFromContext(ctx context.Context) (*client, error) {
	headers, _ := metadata.FromIncomingContext(ctx)
	tokenRaw := headers["authorization"]
	if len(tokenRaw) == 0 {
		return nil, errors.New("no token provided")
	}
	token, err := uuid.Parse(tokenRaw[0])
	if err != nil {
		return nil, errors.New("cannot parse token")
	}
	s.mu.RLock()
	currentClient, ok := s.clients[token]
	s.mu.RUnlock()
	if !ok {
		return nil, errors.New("token not recognized")
	}
	return currentClient, nil
}

// broadcast sends a response to all clients.
func (s *GameServer) broadcast(resp *framework.Response) {
	s.mu.Lock()
	log.Println("Broadcasting")
	for id, currentClient := range s.clients {
		if currentClient.streamServer == nil {
			continue
		}
		if err := currentClient.streamServer.Send(resp); err != nil {
			log.Printf("%s - broadcast error %v", id, err)
			currentClient.done <- errors.New("failed to broadcast message")
			continue
		}
		log.Printf("%s - broadcasted %+v", resp, id)
	}
	s.mu.Unlock()
}

func (s *GameServer) watchTimeout() {
	timeoutTicker := time.NewTicker(1 * time.Minute)
	go func() {
		for {
			for _, client := range s.clients {
				if time.Since(client.lastMessage).Minutes() > clientTimeout {
					client.done <- errors.New("you have been timed out")
					return
				}
			}
			<-timeoutTicker.C
		}
	}()
}

func (s *GameServer) Connect(ctx context.Context, req *framework.ConnectRequest) (*framework.ConnectResponse, error) {
	log.Println("Server: Client Attempting Conenction")

	if len(s.clients) >= maxClients {
		log.Println("Server: Client Connection Refused: Full Server")
		return nil, errors.New("the server is full")
	}

	// Only check to see if username exists and is valid format
	re := regexp.MustCompile("^[a-zA-Z0-9]+$")
	if !re.MatchString(req.Username) {
		log.Println("Bad Username")
		return nil, errors.New("invalid name provided")
	}

	token := uuid.New()

	// Add the player.
	player := &backend.Player{
		Name: req.Username,
		EntityBase: backend.EntityBase{
			UUID: token,
		},
	}

	log.Printf("Server: Adding Client: %s", token.String())
	s.game.Mu.Lock()
	s.game.AddEntity(player)
	s.game.Mu.Unlock()

	// Build a slice of current entities.
	s.game.Mu.RLock()

	entities := make([]*framework.Data, 0)
	for _, entity := range s.game.Entities {
		entities = append(entities, &framework.Data{
			Action: framework.Action_ADD,
			Entity: (*entity).SerializeData(),
		})
	}
	s.game.Mu.RUnlock()

	// Inform all other clients of the new player.
	resp := framework.Response{
		Data: &framework.Data{
			Action: framework.Action_ADD,
			Entity: player.SerializeData(),
		},
	}
	s.broadcast(&resp)

	// Add the new client.
	s.mu.Lock()
	s.clients[token] = &client{
		id:          token,
		done:        make(chan error),
		lastMessage: time.Now(),
	}
	s.mu.Unlock()

	log.Printf("Server: Sending Connection Response to client: %s", token.String())

	return &framework.ConnectResponse{
		Id:       token.String(),
		Entities: entities,
	}, nil
}

// Stream is the main loop for dealing with individual players.
func (s *GameServer) Stream(srv framework.Game_StreamServer) error {
	ctx := srv.Context()
	currentClient, err := s.getClientFromContext(ctx)
	if err != nil {
		return err
	}
	if currentClient.streamServer != nil {
		return errors.New("stream already active")
	}
	currentClient.streamServer = srv

	log.Println("Server: Starting New Stream for Connection")

	// Wait for stream requests.
	go func() {
		for {
			req, err := srv.Recv()
			if err != nil {
				log.Printf("Server: receive error %v", err)
				currentClient.done <- errors.New("failed to receive request")
				return
			}
			log.Printf("Server: got message %+v", req)
			currentClient.lastMessage = time.Now()

			switch req.Data.GetAction() {
			case framework.Action_GET:
				s.handleGetTimeRequest(req, currentClient)
			}
		}
	}()

	// Wait for stream to be done.
	var doneError error
	select {
	case <-ctx.Done():
		doneError = ctx.Err()
	case doneError = <-currentClient.done:
	}
	log.Printf(`Server: stream done with error "%v"`, doneError)

	s.removeClient(currentClient.id)
	s.removePlayer(currentClient.id)

	return doneError
}

func (s *GameServer) handleGetTimeRequest(req *framework.Request, currentClient *client) {
	log.Println("Server: Handling Get Time Request")
}
