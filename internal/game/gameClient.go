package game

import (
	"context"
	"log"
	"reflect"

	"gitlab.com/go-game-engine/engin/internal/backend"
	"gitlab.com/go-game-engine/engin/internal/framework"
	"gitlab.com/go-game-engine/engin/internal/frontend"

	"github.com/google/uuid"
	"google.golang.org/grpc/metadata"
)

type GameClient struct {
	CurrentPlayer uuid.UUID
	Stream        framework.Game_StreamClient
	Game          *backend.Game
	View          *frontend.View
}

// NewGameClient constructs a new game client struct.
func NewGameClient(game *backend.Game, view *frontend.View) *GameClient {
	log.Println("Client: Creating New Game Client")
	return &GameClient{
		Game: game,
		View: view,
	}
}

func (c *GameClient) Connect(grpcClient framework.GameClient, playerName string, password string) error {
	log.Println("Client: Starting New Connection")
	// Connect to server.
	req := framework.ConnectRequest{
		Username: playerName,
		Password: password,
	}

	resp, err := grpcClient.Connect(context.Background(), &req)
	if err != nil {
		return err
	}
	log.Printf("Client: Connection To Server Negotiated with id: %s", resp.Id)

	// Add initial entity state.
	log.Println("Client: Generating Default State from Server")
	for _, entity := range resp.Entities {
		tmpResponse := &framework.Response{
			Data: entity,
		}
		c.HandleResponse(tmpResponse)
	}

	// Initialize stream with token.
	log.Println("Client: Trying to establish Stream connection to server")
	header := metadata.New(map[string]string{"authorization": resp.Id})
	ctx := metadata.NewOutgoingContext(context.Background(), header)
	stream, err := grpcClient.Stream(ctx)
	if err != nil {
		return err
	}
	log.Println("Client: Stream Connection Established")

	log.Printf("Client: Assigning Game Client data: UUID: %s", uuid.MustParse(resp.Id))
	c.CurrentPlayer = uuid.MustParse(resp.Id)
	c.View.CurrentPlayer = uuid.MustParse(resp.Id)
	c.Stream = stream

	return nil
}

// Start begins the goroutines needed to recieve server changes and send game
// changes.
func (c *GameClient) Start() {

	// Handle stream messages.
	log.Println("Client: Starting Handler Function to recieve stream data")
	go func() {
		for {
			resp, err := c.Stream.Recv()
			if err != nil {
				return
			}

			c.HandleResponse(resp)
		}
	}()
}

func (c *GameClient) HandleResponse(resp *framework.Response) {
	c.Game.Mu.Lock()
	log.Println("Client: Recieved New Entity Request from Server")

	anyEntity := resp.GetData().GetEntity()

	m, err := anyEntity.UnmarshalNew()
	if err != nil {
		log.Println("Client: Error")
	}

	handleFunc, ok := DecodeMap[reflect.TypeOf(m)]
	if !ok {
		log.Println("Client: Failed to find data type: ", reflect.TypeOf(m))
	} else {
		handleFunc(c.Game, resp)
	}

	c.Game.Mu.Unlock()
}

// Map of reflect types to the handling functions needed to work with them
// All actions are routed to the same function, therefore that function
// should handle all routing.
var (
	DecodeMap = map[reflect.Type]func(game *backend.Game, resp *framework.Response){
		reflect.TypeOf(&framework.Player{}): backend.PlayerHandleData,
	}
)
