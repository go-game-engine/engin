package frontend

import (
	"gitlab.com/go-game-engine/engin/pkg/primitives"

	"code.rocketnine.space/tslocum/cview"
)

func NewLandingPage(app *cview.Application) cview.Primitive {
	grapicalViewer := primitives.NewGraphicalViewer()

	//Vertical Flex's
	subFlex := cview.NewFlex()
	subFlex.SetDirection(cview.FlexRow)
	subFlex.AddItem(cview.NewBox(), 0, 1, false)
	subFlex.AddItem(grapicalViewer, 0, 3, false)
	subFlex.AddItem(NewTimeKeeper(), 5, 1, false)

	//Horizontal Flex's
	flex := cview.NewFlex()
	flex.AddItem(subFlex, 0, 2, false)
	flex.AddItem(cview.NewBox(), 20, 1, false)

	return flex
}
