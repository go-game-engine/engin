package frontend

import (
	"time"

	"gitlab.com/go-game-engine/engin/internal/backend"

	"code.rocketnine.space/tslocum/cview"
	"github.com/google/uuid"
)

const (
	drawFrequency = 17 * time.Millisecond
)

type View struct {
	Game          *backend.Game
	App           *cview.Application
	CurrentPlayer uuid.UUID
	Done          chan error
}

// NewView construsts a new View struct.
func NewView(game *backend.Game) *View {
	app := cview.NewApplication()

	// Allow Mouse input on UI
	app.EnableMouse(true)

	view := &View{
		Game: game,
		App:  app,
		Done: make(chan error),
	}

	// Create the Login Screen and Next Screen
	LandingPage := NewLandingPage(app)
	LoginScreen := CreateLoginScreen(app, LandingPage)

	app.SetRoot(LoginScreen, true)
	return view
}

// Start starts the frontend game loop.
func (view *View) Start() {
	drawTicker := time.NewTicker(drawFrequency)
	stop := make(chan bool)
	go func() {
		for {
			view.App.Draw()
			<-drawTicker.C
			select {
			case <-stop:
				return
			default:
			}
		}
	}()
	go func() {
		err := view.App.Run()
		stop <- true
		drawTicker.Stop()
		select {
		case view.Done <- err:
		default:
		}
	}()
}
