package frontend

import (
	"code.rocketnine.space/tslocum/cview"
)

func CreateLoginScreen(app *cview.Application, LandingPage cview.Primitive) cview.Primitive {

	mainBox := cview.NewForm()

	mainBox.AddInputField("Username", "", 20, nil, nil)
	mainBox.AddPasswordField("Password", "", 20, '*', nil)
	mainBox.AddCheckBox("", "Remember Me?", false, nil)
	mainBox.AddButton("Login", func() {
		//username := mainBox.GetFormItem(0).(*cview.InputField).GetText()
		//password := mainBox.GetFormItem(1).(*cview.InputField).GetText()
		app.SetRoot(LandingPage, true)
	})
	mainBox.AddButton("Exit", func() {
		app.Stop()
	})
	mainBox.SetTitle("Login Screen")

	//Vertical
	subFlex := cview.NewFlex()
	subFlex.SetDirection(cview.FlexRow)
	subFlex.AddItem(cview.NewBox(), 0, 1, false)
	subFlex.AddItem(mainBox, 0, 1, false)
	subFlex.AddItem(cview.NewBox(), 0, 1, false)

	//Horizontal
	flex := cview.NewFlex()
	flex.AddItem(cview.NewBox(), 0, 1, false)
	flex.AddItem(subFlex, 0, 1, false)
	flex.AddItem(cview.NewBox(), 0, 1, false)

	return flex
}
