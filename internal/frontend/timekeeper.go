package frontend

import (
	"code.rocketnine.space/tslocum/cview"
)

var times = []string{
	"1 min",
	"5 min",
	"1 day",
	"7 days",
	"1 month",
	"6 months",
	"1 year",
	"5 years",
}

func NewTimeKeeper() cview.Primitive {

	// Top Row
	flexTop := cview.NewFlex()

	date := cview.NewTextView()
	date.SetText("[blue]2021[white]/[blue]09[white]/[blue]07 [green]05:58:00[white]")
	date.SetTextAlign(cview.AlignCenter)
	date.SetDynamicColors(true)

	// Center the date text
	flexTop.AddItem(cview.NewBox(), 0, 1, false)
	flexTop.AddItem(date, 0, 1, false)
	flexTop.AddItem(cview.NewBox(), 0, 1, false)

	// Bottom Row
	flexBottom := cview.NewFlex()
	flexBottom.SetBackgroundTransparent(false)

	for _, time := range times {
		button := cview.NewButton(time)
		flexBottom.AddItem(button, 0, 1, false)
		box := cview.NewBox()
		flexBottom.AddItem(box, 1, 1, false)
	}

	// Entire UI Element
	element := cview.NewFlex()
	element.SetDirection(cview.FlexRow)
	element.AddItem(flexTop, 0, 1, false)
	element.AddItem(flexBottom, 0, 1, false)

	return element
}
