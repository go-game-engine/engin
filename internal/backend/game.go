package backend

import (
	"log"
	"sync"

	"github.com/google/uuid"
)

type Game struct {
	Entities map[uuid.UUID]*Entity
	Mu       sync.RWMutex
}

// NewGame constructs a new Game struct.
func NewGame() *Game {
	game := Game{
		Entities: make(map[uuid.UUID]*Entity),
	}
	return &game
}

// Start begins the main game loop, which waits for new actions and updates the
// game state occordinly.
func (game *Game) Start() {
}

// AddEntity adds an entity to the game.
func (game *Game) AddEntity(entity Entity) {
	log.Printf("Adding Entity %s", entity.ID())
	game.Entities[entity.ID()] = &entity
}

// UpdateEntity updates an entity.
func (game *Game) UpdateEntity(entity Entity) {
	log.Printf("Updating Entity %s", entity.ID())
	game.Entities[entity.ID()] = &entity
}

// GetEntity gets an entity from the game.
func (game *Game) GetEntity(id uuid.UUID) *Entity {
	log.Printf("Getting Entity %s", id)
	return game.Entities[id]
}

// RemoveEntity removes an entity from the game.
func (game *Game) RemoveEntity(id uuid.UUID) {
	log.Printf("Removing Entity %s", id)
	delete(game.Entities, id)
}
