package backend

import (
	"log"

	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/anypb"
)

// Identifier is an entity that provides an ID method.
type Entity interface {
	ID() uuid.UUID
	SerializeData() *anypb.Any
}

// IdentifierBase is embedded to satisfy the Identifier interface.
type EntityBase struct {
	UUID uuid.UUID
}

// ID returns the UUID of an entity.
func (e EntityBase) ID() uuid.UUID {
	return e.UUID
}

func (e EntityBase) SerializeData() *anypb.Any {
	log.Println("Serializing Nothing")
	return &anypb.Any{}
}
