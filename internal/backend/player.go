package backend

import (
	"log"

	"gitlab.com/go-game-engine/engin/internal/framework"

	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/anypb"
)

type Player struct {
	Name string
	EntityBase
}

func (p *Player) SerializeData() *anypb.Any {
	log.Println("Serializing Player")
	any, _ := anypb.New(&framework.Player{
		Id: p.UUID.String(),
	})
	return any
}

func PlayerHandleData(game *Game, resp *framework.Response) {

	anyEntity := resp.GetData().GetEntity()

	newPlayer := &framework.Player{}
	err := anyEntity.UnmarshalTo(newPlayer)
	if err != nil {
		log.Println("Client: Error ", err)
	}

	Player := &Player{
		Name: "TESTER",
		EntityBase: EntityBase{
			UUID: uuid.MustParse(newPlayer.GetId()),
		},
	}

	switch resp.GetData().Action {
	case framework.Action_ADD:
		game.AddEntity(Player)
	case framework.Action_DELETE:
		log.Println("Haven't implemented Player Delete")
	case framework.Action_UPDATE:
		log.Println("Haven't implemented Player Update")
	case framework.Action_GET:
		log.Println("Haven't implemented Player Get")
	}
}
