package network

import "google.golang.org/grpc"

var (
	directory = "/home/joseph/Desktop/example/"

	CLIENT_CERT_PATH = directory + "assets/certs/client/client.pem"
	CLIENT_KEY_PATH  = directory + "assets/certs/client/client.key"
	SERVER_CERT_PATH = directory + "assets/certs/server/server.pem"
	SERVER_KEY_PATH  = directory + "assets/certs/server/server.key"

	BufferSize = 512

	grpcServer *grpc.Server
)
