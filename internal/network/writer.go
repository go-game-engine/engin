package network

import (
	"crypto/tls"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func StartWriterServer() *grpc.ClientConn {
	// Load the Client Cert for Mutual TLS
	cert, err := tls.LoadX509KeyPair(CLIENT_CERT_PATH, CLIENT_KEY_PATH)
	if err != nil {
		log.Fatalf("server: loadkeys: %s", err)
	}

	// Configuration for the TLS connection
	config := tls.Config{Certificates: []tls.Certificate{cert}, InsecureSkipVerify: true}

	// Connect to the Listen Server using the config
	ClientConn, err := grpc.Dial("127.0.0.1:8000", grpc.WithTransportCredentials(credentials.NewTLS(&config)))
	if err != nil {
		log.Fatalf("client: dial: %s", err)
	}
	log.Println("client: connected to: ", ClientConn.Target())

	return ClientConn
}
