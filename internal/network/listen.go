package network

import (
	"crypto/rand"
	"crypto/tls"
	"log"
	"net"

	"google.golang.org/grpc"
)

func StartTLSListenServer(port string) net.Listener {
	// Read Server Cert Files
	cert, err := tls.LoadX509KeyPair(SERVER_CERT_PATH, SERVER_KEY_PATH)
	if err != nil {
		log.Fatalf("server: loadkeys: %s", err)
	}

	// Create configuration for server conenction
	config := tls.Config{Certificates: []tls.Certificate{cert}}

	// Create a random number for connections
	config.Rand = rand.Reader

	// Start listening server
	service := "0.0.0.0" + port
	listener, err := tls.Listen("tcp", service, &config)
	if err != nil {
		log.Fatalf("server: listen: %s", err)
	}
	log.Print("server: listening")

	return listener
}

func CreateGRPCServer() *grpc.Server {
	// Create a New grpc Server
	grpcServer = grpc.NewServer()
	return grpcServer
}

func GetGRPCServer() *grpc.Server {
	return grpcServer
}

func StartGRPCServer(listener net.Listener) {
	log.Println("Server: Starting gRPC Server")
	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("server: failed to serve: %s", err)
	}
}
