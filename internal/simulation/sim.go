package backend

import (
	"time"
)

var (
	UniverseSim *Simulation
)

type Simulation struct {
	Date    time.Time
	Planets map[string]*Planet
}

type Planet struct {
	Name       string
	PosX, PosY int
}

func CreateSimulation() {
	UniverseSim = &Simulation{
		Planets: map[string]*Planet{},
	}
	star := &Planet{
		Name: "star",
		PosX: 0,
		PosY: 0,
	}
	planet1 := &Planet{
		Name: "one",
		PosX: 1,
		PosY: 0,
	}
	planet2 := &Planet{
		Name: "two",
		PosX: 2,
		PosY: 0,
	}

	UniverseSim.Date = time.Date(3021, time.April, 24, 8, 43, 58, 0, time.UTC)

	UniverseSim.Planets[star.Name] = star
	UniverseSim.Planets[planet1.Name] = planet1
	UniverseSim.Planets[planet2.Name] = planet2
}

func GetDate() time.Time {
	return UniverseSim.Date
}

func AdvanceTime(dDate time.Time, dTime time.Duration) {
	UniverseSim.Date = UniverseSim.Date.Add(dTime)
	y, m, d := dDate.Date()
	UniverseSim.Date.AddDate(y, int(m), d)
}
