package primitives

import (
	"fmt"
	"strings"

	"code.rocketnine.space/tslocum/cview"
	"github.com/gdamore/tcell/v2"
)

const circleShape = `    *  *
 *        *
*          *
*          *
 *        *
    *  *`

// Planet implements a simple primitive for radio button selections.
type Planet struct {
	*cview.Box
	XPos, YPos int
}

// NewPlanet returns a new radio button primitive.
func NewPlanet() *Planet {
	myNewPlanet := &Planet{
		Box:  cview.NewBox(),
		XPos: 0,
		YPos: 0,
	}
	return myNewPlanet
}

// Draw draws this primitive onto the screen.
func (r *Planet) Draw(screen tcell.Screen) {
	r.Box.SetBackgroundTransparent(true)
	r.Box.Draw(screen)
	x, y, width, _ := r.GetInnerRect()

	stringArray := strings.Split(circleShape, "\n")
	for dy, PlanetSection := range stringArray {
		cview.Print(screen, []byte(PlanetSection), x+r.XPos, y+dy+r.YPos, width, cview.AlignLeft, tcell.ColorYellow.TrueColor())
	}

}

// MouseHandler returns the mouse handler for this primitive.
func (r *Planet) MouseHandler() func(action cview.MouseAction, event *tcell.EventMouse, setFocus func(p cview.Primitive)) (consumed bool, capture cview.Primitive) {
	return r.WrapMouseHandler(func(action cview.MouseAction, event *tcell.EventMouse, setFocus func(p cview.Primitive)) (consumed bool, capture cview.Primitive) {
		Boxx, Boxy, _, _ := r.GetInnerRect()
		eventx, eventy := event.Position()

		localeventx, localeventy := eventx-Boxx-r.XPos, eventy-Boxy-r.YPos

		stringArray := strings.Split(circleShape, "\n")

		hit := false

		if 0 <= localeventy && localeventy < len(stringArray) {
			if 0 <= localeventx && localeventx < len(stringArray[localeventy]) {
				hit = true
			}
		}

		if !hit {
			return false, nil
		}

		// Process mouse event.
		if action == cview.MouseLeftClick {
			setFocus(r)
			fmt.Println(localeventx, localeventy)
			consumed = true
		}

		return
	})
}
