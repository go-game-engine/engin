package primitives

import (
	"code.rocketnine.space/tslocum/cview"
)

type GraphicalViewer struct {
	*cview.WindowManager
}

func NewGraphicalViewer() *GraphicalViewer {

	wm := cview.NewWindowManager()
	wm.SetBorder(true)
	myGraphicalViewer := &GraphicalViewer{
		WindowManager: wm,
	}
	return myGraphicalViewer
}

func (gv *GraphicalViewer) Add(w ...*cview.Window) {
	gv.WindowManager.Add(w...)
	for _, window := range w {
		window.SetBorder(false)
		window.SetRect(10, 20, 12, 6)
		window.SetBackgroundTransparent(true)
	}
}
