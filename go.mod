module gitlab.com/go-game-engine/engin

go 1.17

require (
	code.rocketnine.space/tslocum/cview v1.5.7
	github.com/gdamore/tcell/v2 v2.4.1-0.20210828201608-73703f7ed490
	github.com/google/uuid v1.3.0
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)

require (
	code.rocketnine.space/tslocum/cbind v0.1.5 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14-0.20210830053702-dc8fe66265af // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210906170528-6f6e22806c34 // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210909211513-a8c4777a87af // indirect
)
