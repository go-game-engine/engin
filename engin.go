package engin

import (
	"net"

	"gitlab.com/go-game-engine/engin/internal/backend"
	"gitlab.com/go-game-engine/engin/internal/framework"
	"gitlab.com/go-game-engine/engin/internal/frontend"
	"gitlab.com/go-game-engine/engin/internal/game"
	"gitlab.com/go-game-engine/engin/internal/network"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func StartTLSListenServer(port string) net.Listener {
	return network.StartTLSListenServer(port)
}

func CreateGRPCServer() *grpc.Server {
	return network.CreateGRPCServer()
}

func NewGame() *backend.Game {
	return backend.NewGame()
}

func RegisterGameServer(grpcServer *grpc.Server, myGame *backend.Game) {
	framework.RegisterGameServer(grpcServer, game.NewGameServer(myGame))
	reflection.Register(grpcServer)
}

func StartGRPCServer(listener net.Listener) {
	network.StartGRPCServer(listener)
}

func StartWriterServer() framework.GameClient {
	gRPCServer := network.StartWriterServer()
	return framework.NewGameClient(gRPCServer)
}

func NewView(game *backend.Game) *frontend.View {
	return frontend.NewView(game)
}

func NewGameClient(myGame *backend.Game, myView *frontend.View) *game.GameClient {
	return game.NewGameClient(myGame, myView)
}
